## Getting Started

Clone this project using the following commands:

```
git clone https://gitlab.com/tolujimoh/php-test.git
cd php-test
```

### Configure the application

Install the project dependencies:

```
composer install
```

### Run the application

From the project directory, run:

```
php -S 127.0.0.1:8080 -t public
```

Then open `http://localhost:8080` and you will see the application.
 

# ukfast/pokédex

**This project will be based primarily on your ability to fulfill the task 
requirements. Any potential design skills are a bonus, but usability, 
performance and security will be taken into account.**

## Introduction
This project provides a starting point which will allow you to create your own 
web-based encyclopedia based on the popular franchise Pokémon - also known as 
a pokédex.

## Project Requirements
To get started, you'll need the following:

 - PHP
 - [Composer](https://getcomposer.org/)
 - git
 
 You are free to use whatever PHP packages and front-end libraries that you 
 wish.



## Task Requirements
To order to complete this challenge, you MUST create a pokédex with minimal 
functionality. Your solution MUST allow the user to browse the full list of 
pokémon in a convenient manner, as well as offer some form of search 
functionality. Your solution MUST also display basic information for a 
specific pokémon, including:

 - At least one image of the pokémon
 - Name
 - Species
 - Height and weight
 - Abilities
 
A RESTful API is available at [Pokéapi](https://pokeapi.co/) which will 
provide you with all the data that you will need. You do not need to create 
an account nor authenticate in order to consume the API, however please 
be aware that this API is rate-limited.
 
To get started, we've given you a skeleton folder structure. It is advised 
that you spend no more than two to three hours on this assignment.

## Submission
To submit your solution, please fork this repository and provide us a link 
to your finished version.

## Copyright
All trademarks as the property of their respective owners.

