<?php

use Src\Services\PokeApiService;

$pokeApiService = new PokeApiService();

try {


    if(is_null($_GET['id']) || !is_numeric ($_GET['id']))
        if(is_null($_GET['name']))
            throw new Exception('Provide valid Pokemon ID');

    $id = is_numeric($_GET['id']) ? $_GET['id']: htmlspecialchars($_GET['name']);
    $limit = 20;

    $pokemonDetails = $pokeApiService->getPokemonDetails($id);

    if(!$pokemonDetails)
        throw new Exception('Invalid Pokemon ID');

} catch (Exception $e) {
    $error = true;
    $errorMessage = $e->getMessage();
}

?>
<html lang="en">
<?php include(__DIR__ . '/../partials/head.php'); ?>
<body>
<div class="container">

    <div class="nav-scroller py-1 mb-2 border-bottom">

    </div>

</div>

<main role="main" class="container">
    <div class="row">
        <div class="col-md-12 page-main">
            <div class="px-3 py-3 pt-md-4 pb-md-4 mx-auto text-center">
                <h1 class="display-4">Pokedex</h1>

            </div>
            <?php if ($error){ ?>
                <div class="card p-4 bg-danger">
                    <p class="text-white"><?= $errorMessage ?></p>
                </div>
            <?php }else{ ?>
                <div class="row no-gutter">
                    <div class="col-md-4">
                        <div class="card">
                            <img src="<?php echo $pokemonDetails->sprites->front_default ?>" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title"><?php echo $pokemonDetails->name ;?></h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <ul class="list-group mb-3">
                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                <div>
                                    <h6 class="my-0"><?php echo ucwords($pokemonDetails->name) ;?></h6>
                                    <small class="text-muted">NAME</small>
                                </div>

                            </li>
                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                <div>
                                    <h6 class="my-0"><?php echo ucwords($pokemonDetails->species->name) ;?></h6>
                                    <small class="text-muted">SPECIES</small>
                                </div>

                            </li>
                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                <div>
                                    <h6 class="my-0"><?php echo $pokemonDetails->height ;?></h6>
                                    <small class="text-muted">HEIGHT</small>
                                </div>

                            </li>

                            <li class="list-group-item d-flex justify-content-between bg-light">
                                <div class="text-success">
                                    <h6 class="my-0"><?php echo $pokemonDetails->weight ;?></h6>
                                    <small>WEIGHT</small>
                                </div>

                            </li>
                            <li class="list-group-item d-flex justify-content-between lh-condensed">
                                <div>
                                    <div class="my-0"><?php  foreach ($pokemonDetails->abilities as $ability){ ?><span class="badge badge-dark"><?php echo $ability->ability->name ?></span> <?php } ?></div>
                                    <small class="text-muted">ABILITIES</small>
                                </div>

                            </li>
                        </ul>
                        <a href="?page=1" class="btn btn-info">Go back to Pokemon List</a>
                    </div>

                </div>
            <?php } ?>
        </div><!-- /.page-main -->

    </div><!-- /.row -->
    <?php include(__DIR__ . '/../partials/footer.php'); ?>
</main><!-- /.container -->
</body>
</html>