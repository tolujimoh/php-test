<?php

use Src\Services\PokeApiService;

$pokeApiService = new PokeApiService();

$IMAGE_PATH = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/';

try {
    $page = !is_null($_GET['page']) && is_numeric ($_GET['page'])? (int)$_GET['page']: 1;

    $limit = 20;
    $offset = ($page - 1) * $limit;

    $pokemonList = $pokeApiService->getPokemonList($offset, $limit);

    $pageCount = floor(($pokemonList->count / $limit) + 1);

    if($page > $pageCount)
        throw new Exception('Invalid Page Number');

} catch (Exception $e) {
    $error = true;
    $errorMessage = $e->getMessage();
}

?>
<html lang="en">
<?php include(__DIR__ . '/../partials/head.php'); ?>
<body>
<div class="container">

    <div class="nav-scroller py-1 mb-2 border-bottom">

    </div>

</div>

<main role="main" class="container">
    <div class="row">
        <div class="col-md-12 page-main">
            <div class="px-3 py-3 pt-md-4 pb-md-4 mx-auto text-center">
                <h1 class="display-4">Pokedex</h1>
                <p class="lead">Here is a list of Pokemons. Click on a card for more details</p>
                <form class="form-inline justify-content-center">
                    <div class="form-group mx-sm-3 mb-2">
                        <input type="text" class="form-control" id="searchQuery" name="searchQuery" required placeholder="Enter name of pokemon">
                    </div>
                    <button type="submit" class="btn btn-primary mb-2">Search</button>
                </form>
            </div>
            <?php if ($error){ ?>
                <div class="card p-4 bg-danger">
                    <p class="text-white"><?= $errorMessage ?></p>
                </div>
            <?php }else{ ?>
                <div class="card-columns">
                    <?php foreach ($pokemonList->results as $pokemon) {
                        // Getting the Id of Pokemon

                        $idx = substr($pokemon->url, strrpos( $pokemon->url,'/', -2) + 1, -1);

                        ?>
                        <a href="?id=<?php echo $idx ?>" class="card p-3">
                            <div class="row no-gutters">
                                <div class="col-xs-4">

                                    <img src="<?php echo $IMAGE_PATH . $idx . '.png' ?>"
                                         class="card-img-top" alt="">
                                </div>
                                <div class="col-xs-8">
                                    <blockquote class="blockquote mb-0 card-body">
                                        <h5 class="m-0 align-middle"><?php echo ucwords($pokemon->name) ?></h5>
                                    </blockquote>
                                </div>
                            </div>

                        </a>
                    <?php } ?>

                </div>
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center mt-5">
                        <li class="page-item <?php echo $page == 1? 'disabled':'' ?>">
                            <a class="page-link" href="?page=1" >Start</a>
                        </li>
                        <?php
                        for($pageNumber = max(1, $page - 3); $pageNumber <= min($pageCount, $page + 3); $pageNumber++ ){
                            if($page - $pageNumber == 3){?>
                                <li class="page-item"><a class="page-link" href="?page=<?php echo floor((1 + $pageNumber)/2) ?>">...</a></li>
                            <?php }else if($pageNumber - $page == 3){?>
                                <li class="page-item"><a class="page-link" href="?page=<?php echo floor(($pageCount + $pageNumber)/2) ?>">...</a></li>
                            <?php }else{?>
                                <li class="page-item <?php echo $page == $pageNumber? 'active':'' ?>"><a class="page-link" href="?page=<?php echo $pageNumber ?>"><?php echo $pageNumber ?></a></li>

                                <?php
                            }
                        } ?>

                        <li class="page-item <?php echo $page == $pageCount? 'disabled':'' ?>">
                            <a class="page-link" href="?page=<?php echo $pageCount ?>">End</a>
                        </li>
                    </ul>
                </nav>
            <?php } ?>
        </div><!-- /.page-main -->

    </div><!-- /.row -->
    <?php include(__DIR__ . '/../partials/footer.php'); ?>
</main><!-- /.container -->
</body>
</html>