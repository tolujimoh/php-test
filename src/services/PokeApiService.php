<?php

namespace Src\Services;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * @property Client pokeClient
 */

class PokeApiService {

    private $pokeClient;

    /**
     * PokeApiService constructor.
     */
    public function __construct()
    {
        $apiUrlBase   = 'https://pokeapi.co/api/v2/';
        $this->pokeClient = new Client(['base_uri' => $apiUrlBase]);

    }

    /**
     * @param int $offset
     * @param int $limit
     * @return mixed
     * @throws GuzzleException
     */
    public function getPokemonList($offset = 0, $limit = 20){

        $response = $this->pokeClient->request('GET', 'pokemon', [
            'query' => [
                'offset' => $offset, 'limit' => $limit
            ],
            'http_errors' => false
        ]);


        if ($response->getStatusCode() !== 200)
            throw new Exception('Unable to get Pokemon List');


        return json_decode($response->getBody());

    }

    /**
     * @param $id
     * @return mixed
     * @throws GuzzleException
     */
    public function getPokemonDetails($id){

        $response = $this->pokeClient->request('GET', 'pokemon/' . $id, [
            'http_errors' => false
        ]);

        if ($response->getStatusCode() !== 200)
            throw new Exception('Unable to get Pokemon Details');

        return json_decode($response->getBody());
    }

    /**
     * @param $name
     * @param int $offset
     * @param int $limit
     * @return \stdClass
     * @throws GuzzleException
     */
    public function findPokemon($name, $offset = 0, $limit = 20){

        try{
            $pokemonList = $this->getPokemonList(0, 1000);



            $queryResults = array_filter($pokemonList->results, function($pokemon) use ($name) {
                return ( strpos(preg_replace('/-|\s/','',$pokemon->name), preg_replace('/-|\s/','',strtolower($name))) !== false );
            });

            $queryList = new \stdClass();
            $queryList->count = count($queryResults);
            $queryList->results = array_slice($queryResults, $offset, $limit);

            return $queryList;

        }catch (Exception $e){
            throw new Exception('Unable to find Pokemon');
        }

    }

}