<?php

include '../bootstrap.php';

if(isset($_GET['searchQuery'])){
    include(__DIR__ . '/../src/views/pokedex/search.php');
    die();
}

if(isset($_GET['id']) || isset($_GET['name']) ){
    include(__DIR__ . '/../src/views/pokedex/detail.php');
    die();
}

include(__DIR__ . '/../src/views/pokedex/list.php');


