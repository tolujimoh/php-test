<?php


use PHPUnit\Framework\TestCase;
use Src\Services\PokeApiService;

class PokeAPIServiceTest extends TestCase
{

    /**
     * @var PokeApiService
     */
    private $pokeApiService;

    public function setUp()
    {
        $this->pokeApiService = new PokeApiService();
    }


    public function testGetPokemonListReturnsResultObject()
    {
        $pokemonList = $this->pokeApiService->getPokemonList();

        $this->assertObjectHasAttribute('results', $pokemonList);
        $this->assertObjectHasAttribute('count', $pokemonList);
    }



    public function testGetPokemonDetailsReturnsResultObject()
    {
        $pokemonDetails = $this->pokeApiService->getPokemonDetails(8);


        $this->assertObjectHasAttribute('abilities', $pokemonDetails);
        $this->assertObjectHasAttribute('name', $pokemonDetails);
        $this->assertEquals('wartortle', $pokemonDetails->name);
        $this->assertObjectHasAttribute('weight', $pokemonDetails);
        $this->assertObjectHasAttribute('height', $pokemonDetails);
        $this->assertObjectHasAttribute('species', $pokemonDetails);
    }

    public function testGetPokemonDetailsThrowsExceptionOnInvalidArgumentTypes()
    {
        $this->expectException(\Exception::class);
        $this->pokeApiService->getPokemonDetails('notRealPokemon');
    }

    public function testFindPokemonReturnsResultObject()
    {
        $pokemonList = $this->pokeApiService->findPokemon('chlorophyll');

        $this->assertObjectHasAttribute('results', $pokemonList);
        $this->assertObjectHasAttribute('count', $pokemonList);
    }

    public function testFindPokemonThrowsExceptionOnInvalidArgumentTypes()
    {
        $this->expectException(\Exception::class);
        $this->pokeApiService->findPokemon('chlorophyll', 'offsetShouldBeInteger', 'limitShouldBeInteger');
    }



    public function tearDown()
    {
        $this->pokeApiService = null;
    }
}